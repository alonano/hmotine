/* 
 * The MIT License
 *
 * Copyright 2017 isokissa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

var machine = require("../src/hmotine.js");

describe("The machine", function() {

    beforeEach(function() {
        machine.clean();
    });

    describe("Function addState()", function() {
        var dummyAdvance; 
        var NAME;

        beforeEach(function() {
            dummyAdvance = function() {};
            NAME = "MY-STATE";
        });

        it("throws if no name is given", function() {
            expect(function() {
                machine.addState(); 
            }).toThrow("State must have the name");
        });

        it("throws if no advance() function is given", function() {
            expect(function() {
                machine.addState(NAME);
            }).toThrow("State must have the advance() function");
        });

        it("throws if the state with the same name already exists", function() {
            expect(function() {
                machine.addState(NAME, dummyAdvance);
                machine.addState(NAME, dummyAdvance);
            }).toThrow("State '" + NAME + "' is already defined");
        });

        it("throws if parent state does not exist", function() {
            var PARENT = "PARENT-STATE"
            expect(function() {
                machine.addState(NAME, dummyAdvance, {}, PARENT);
            }).toThrow("The parent state '" + PARENT + "' is undefined");
        });

        it("succeeds if parent state is correct", function() {
            var PARENT_DATA = 22; 
            var CHILD_DATA_1 = 55;
            var CHILD_DATA_2 = 234;
            machine.addState("parent",
                    dummyAdvance, {parentData: PARENT_DATA});
            machine.addState("child1",
                    dummyAdvance, {childData1: CHILD_DATA_1}, "parent");
            machine.addState("child2",
                    dummyAdvance, {childData1: CHILD_DATA_2}, "parent");
            expect(machine.states["child1"].parent.name).toEqual("parent");
        });

    });

    describe("When invoking start()", function() {

        it("throws if start state is not given", function() {
            expect(function() {
                machine.start();
            }).toThrow("hmotine start state not given");
        });

        it("throws if start state does not exist", function() {
            var NON_EXISTENT_STATE = "abcde"
            expect(function() {
                machine.start(NON_EXISTENT_STATE);
            }).toThrow("Tried to start from nonexisting state '" + NON_EXISTENT_STATE + "'");
        });
        
        describe("if state's advance function does not return the needed object", function() {

            it("stops the machine and throws", function() {
                var INVALID = "adsfasdf"
                machine.addState(INVALID, function() {});
                machine.state = machine.states[INVALID];
                expect(function() {
                    machine.start(INVALID);
                }).toThrow("The advance of state '" + INVALID + "' does not return correct state transition object");
                expect(machine.running()).toEqual(false);
            });

        });

        describe("if in a state MY_STATE", function() {
            var MY_STATE = "MY_STATE";
            var addMyTestState = function(name, endBehavior) {
                machine.addState(name, function(m) {return endBehavior(m);});
            };

            it("invokes also MY_STATE's advance() function", function () {
                addMyTestState(MY_STATE, function(m) { return m.keep(65);});
                spyOn(machine.states[MY_STATE], "advance").
                        and.returnValue({
                    nextState: machine.states[MY_STATE]
                });

                machine.start(MY_STATE);

                expect(machine.states[MY_STATE].advance).toHaveBeenCalled();
            });

            it("stays in same state with keep()", function() {
                addMyTestState(MY_STATE, function(m) { return m.keep(65);});
                machine.start(MY_STATE);
                expect(machine.state).toBe(machine.states[MY_STATE]);
            });
            
            it("switches the state with goto()", function() {
                var NEXT_STATE = "NEXT_STATE";
                addMyTestState(MY_STATE, function(m) { return m.goto(NEXT_STATE, 65);});
                addMyTestState(NEXT_STATE, function(m) { return m.keep(65);});
                machine.start(MY_STATE);
                expect(machine.state).toBe(machine.states[NEXT_STATE]);

            });
            it("throws if next state does not exist", function() {
                var NON_EXISTING_STATE = "abcd";
                addMyTestState(MY_STATE, function(m) { return m.goto(NON_EXISTING_STATE);});
                expect(function() {
                    machine.start(MY_STATE);                
                }).toThrow("Tried to switch to nonexisting '" + NON_EXISTING_STATE + "'");
                expect(machine.running()).toEqual(false);
            });        

            it("ends the execution with end()", function() {
                var NEXT_STATE = "NEXT_STATE";
                addMyTestState(MY_STATE, function(m) { return m.end();});
                machine.start(MY_STATE);
                expect(machine.running()).toEqual(false);
            });

        });

    });

    describe("Test machine that counts from 0 to 5 and stops", function() {
        var HOW_MANY_TIMES;
        var realSetTimeout = setTimeout; 
        
        beforeEach(function() {
            HOW_MANY_TIMES = 5;
            jasmine.getGlobal().setTimeout = function(func, t, x) {
                func(x);
            }
        });
        
        afterEach(function() {
            jasmine.getGlobal().setTimeout = realSetTimeout; 
        });

        it("works", function () {
            machine.addState("INCDEC", function() {}, { counter: 0 });    
            machine.addState("INCREMENT", 
                function(m) {
                    dummy(m.get("counter"));
                    if (m.get("counter") < HOW_MANY_TIMES) {
                        m.set("counter", m.get("counter") + 1);
                        return m.keep();
                    } else {
                        return m.end();
                    }
                },
                {},
                "INCDEC"
            );
            var dummy = jasmine.createSpy("dummy");
            machine.start("INCREMENT");
            expect(dummy).toHaveBeenCalledWith(0);
            expect(dummy).toHaveBeenCalledWith(1);
            expect(dummy).toHaveBeenCalledWith(2);
            expect(dummy).toHaveBeenCalledWith(3);
            expect(dummy).toHaveBeenCalledWith(4);
            expect(dummy).toHaveBeenCalledWith(5);
            expect(machine.running()).toEqual(false);
        });

    });

    describe("When invoked next state advance()", function() {

        beforeEach(function() {
            jasmine.clock().install();
        });

        afterEach(function() {
            jasmine.clock().uninstall();
        });

        it("happens exactly T milliseconds after the start of first invocation", function() {
            var dummy = jasmine.createSpy("dummy");
            machine.addState("FIRST", 
                function(m) {
                    return m.goto("SECOND", 100);
                }
            );
            machine.addState("SECOND", 
                function(m) {
                    dummy();
                    return m.end();
                }
            );
            machine.start("FIRST");

            expect(machine.state).toBe(machine.states.SECOND);
            jasmine.clock().tick(69);
            expect(dummy).not.toHaveBeenCalled();
            jasmine.clock().tick(31);
            expect(dummy).toHaveBeenCalled();
            expect(machine.running()).toEqual(false);
        });

    });

});

