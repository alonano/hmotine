/* 
 * The MIT License
 *
 * Copyright 2017 isokissa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

var templateState = require("../src/state.js");

describe("State created with no parameters", function() {
     
    it("will be called default state", function(){
        var state = templateState.createInstance();
        expect(state.name).toEqual("default");
    }); 

    describe("When invoking advance()", function() {
        
        it("will throw exception", function() {
            var state = templateState.createInstance("dummy");
            expect(function() {
                state.advance();
            }).toThrow("default state should not be used");
        });
        
    });

});

describe("State with data", function() {
    var dummyAdvance; 
    
    beforeEach(function() {
        dummyAdvance = function() {};
    })
    
    it("can access the data under its 'data' object", function() {
        var VALUE = 33;
        var state = templateState.createInstance("name", dummyAdvance, {stateData: VALUE});

        expect(state.getS("stateData")).toEqual(VALUE);
    });
    
    it("throws if there is no parameter", function() {
        var NONEXISTING = "ABCD";
        var state = templateState.createInstance("name", dummyAdvance, {missed: 0});
        expect(function() {
            state.getS(NONEXISTING);
        }).toThrow("Not found parameter '" + NONEXISTING + "' in state '" + 
                state.name + "'");
        
    });

    describe("and with parent", function() {
        var grandParent; 
        var GRAND_PARENT_KEY = "grandParentKey";
        var GRAND_PARENT_VALUE = 234234;
        var parent;
        var PARENT_KEY = "parentKey";
        var PARENT_VALUE = 343;
        var INVALID_PARAMETER = "non-existing-parent-data";
        var OWN_KEY = "ownKey";
        var OWN_VALUE = 6556654;
        var state; 

        beforeEach(function() {
            grandParent = templateState.createInstance("grandParent", dummyAdvance, 
                                                       {[GRAND_PARENT_KEY]: GRAND_PARENT_VALUE});
            parent = templateState.createInstance("parent", dummyAdvance, 
                                                  {[PARENT_KEY]: PARENT_VALUE}, 
                                                  grandParent);
            state = templateState.createInstance("state", dummyAdvance, 
                                                  {[OWN_KEY]: OWN_VALUE}, parent);
        });

        it("can get own data", function() {
            expect(state.getS(OWN_KEY)).toEqual(OWN_VALUE);
        });

        it("can set own data", function() {
            state.setS(OWN_KEY, "new");
            expect(state.getS(OWN_KEY)).toEqual("new");
        });

        it("can get parent's data", function() {
            expect(state.getS(PARENT_KEY)).toEqual(PARENT_VALUE);
        });
        
        it("can get parent's parent data", function() {
            expect(state.getS(GRAND_PARENT_KEY)).toEqual(GRAND_PARENT_VALUE);
        });
        
        it("throws if data is not available in parent for getting", function() {
            expect(function() {
                state.getS(INVALID_PARAMETER);
            }).toThrow("Not found parameter '" + INVALID_PARAMETER + 
                       "' in state '" + state.name + "'");
        });
        
        it("can set the parent's data", function() {
            var NEW_PARENT_STATE = 535435;
            state.setS(PARENT_KEY, NEW_PARENT_STATE);
            expect(state.getS(PARENT_KEY)).toEqual(NEW_PARENT_STATE);
        });
        
        it("can set the parent's parent data", function() {
            var NEW_GRANDPARENT_STATE = 2323432;
            state.setS(GRAND_PARENT_KEY, NEW_GRANDPARENT_STATE);
            expect(state.getS(GRAND_PARENT_KEY)).toEqual(NEW_GRANDPARENT_STATE);
        });
        
        it("cannot set the non-existing parent data", function() {
            expect(function() {
                state.setS(INVALID_PARAMETER); 
            }).toThrow("Not found parameter '" + INVALID_PARAMETER + "' in state '" + 
                       state.name + "'");
        });
        
        it("can get all data", function() {
            var all = state.getAllData();
            expect(Object.keys(all).length).toEqual(3);
            expect(all[GRAND_PARENT_KEY]).toEqual(GRAND_PARENT_VALUE);
            expect(all[PARENT_KEY]).toEqual(PARENT_VALUE);
            expect(all[OWN_KEY]).toEqual(OWN_VALUE);
        });
        
    });
    
});




