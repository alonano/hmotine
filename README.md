# hmotine

Real-Time hierarchical (in)finite state machine framework. 

The states are hierarchical, the state transition can happen in timed manner. 

Look at https://gitlab.com/alonano/kapsula for an example. 

### Install hmotine 

    npm install hmotine

### Using hmotine

#### Registering states

Register as many states as you want. A state is defined by: 

* State _name_, a valid Javascript identifier
* State _advance_ function, a function that implements the transition logic for 
    the state. It accepts one parameter, the pointer to hmotine machine object. 
    Through this pointer, _advance_ function can access utility functions provided by 
    the machine. 
* State _object_, an object containing variables and functions that can be accessed
    from state's _advance_ function, or from child state's _advance_ functions. 
* _Parent_ state name. Any already defined state can be the parent of a newly 
    defined state. This is why the order of state definitions matters. 

#### Advance function

_Advance_ function of a state will be invoked when machine is in that state. 
The _advance_ function can access its own _object_ and _objects_ of its parents, 
and it can update the _object_ variables with new values. _Advance_ function
in the end of its execution has to instruct the machine what will be the next state. 
_Advance_ function takes one parameter, the hmotine machine. In the rest of this 
chapter we assume that the name of that parameter is `m`.

##### Accessing state variables

The hmotine machine provides the method for _advance_ function to access the state. 
It will first look at state's own properties, and if not found it will look at
state's parent's properties and so on, up the chain of parent-child relationship. 

The methods to access and modify the state are `m.get()` and `m.set()`. 

##### State transition

In the end of its execution flow, _advance_ function should instruct the machine 
what to do. Possible options: 

* Stay in the current state: `return m.keep()`
* Go to other state: `return m.goto("NEXT_STATE")`
* Stop the machine: `return m.end()`

##### Timing of the state transition

Functions for state transition `keep`, `goto` and `end`, that are used to finish 
the _advance_ function can have additional parameter which says in how many 
milliseconds should execute the _advance_ function next time. 
For example, if _advance_ function is finished by `return m.goto("NEXT_STATE, 200)`, 
the _advance_ function of `NEXT_STATE` will be invoked after 200-x milliseconds, where
x is the number of milliseconds taken by previous _advance_ function. 

#### Debug mode

Use `machine.debugOn()` to enable debug mode. In debug mode, hmotine will log to the
console the current state and all state variables

#### Code example

    var machine = require("hmotine");

    machine.addState("root", function(m) {}, {c: 0});

    machine.addState("up", function(m) {
        m.set("c", m.get("c") + 1);
        console.log(m.get("c"));
        if (m.get("c") < 10) {
            return m.keep(500);
        } else {
            return m.goto("down", 2000);
        }
    }, {}, "root");

    machine.addState("down", function(m) {
        m.set("c", m.get("c") - 1);
        console.log(m.get("c"));
        if (m.get("c") > 0) {
            return m.keep(500);
        } else {
            return m.goto("up", 2000);
        }
    }, {}, "root");

    machine.start("up");

