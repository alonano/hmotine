/* 
 * The MIT License
 *
 * Copyright 2017 isokissa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

var baseState = {
    createInstance: function(name, advanceFn, fields, parentState) {
        var newState = Object.create(this);
        newState.data = fields || {};
        newState.name = name || "default"; 
        newState.advance = advanceFn || function() {
            throw "default state should not be used";        
        };
        newState.parent = parentState;
        return newState;
    },
        
    getS: function(parameter) {
        var current = this; 
        while (!!current) {
            if (current.data.hasOwnProperty(parameter)) {
                return current.data[parameter];
            }
            current = current.parent; 
        }
        throw "Not found parameter '" + parameter + "' in state '" + this.name + "'";
    },
    
    setS: function(parameter, value) {
        var current = this; 
        while (current) {
            if (current.data.hasOwnProperty(parameter)) {
                current.data[parameter] = value; 
                return; 
            }
            current = current.parent; 
        }
        throw "Not found parameter '" + parameter + "' in state '" + 
              this.name + "'";
    },
    
    getAllData: function() {
        var d = {};
        var current = this; 
        while (!!current) {
            for (k in current.data) {
                d[k] = current.data[k];
            }
            current = current.parent; 
        }
        return d;
    },
    
    data: {}
            
}; 

module.exports = baseState; 

