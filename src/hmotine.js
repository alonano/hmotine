/* 
 * The MIT License
 *
 * Copyright 2017 isokissa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

var state = require("./state.js");

var machine = {
    addState: function(name, advanceFn, data, parentName) {
        if (!name) {
            throw "State must have the name";
        } else if (this.states[name]) {
            throw "State '" + name + "' is already defined";
        }
        if (!advanceFn) {
            throw "State must have the advance() function"; 
        }
        if (parentName && !this.states[parentName]) {
            throw "The parent state '" + parentName + "' is undefined";
        }
        var parent = parentName && this.states[parentName];        
        var newState = state.createInstance(name, advanceFn, data, parent);
        this.states[name] = newState; 
    }, 
    
    start: function(startState) {
        if (!startState) {
            throw "hmotine start state not given";
        }
        if (!this.states[startState]) {
            this.state = null;
            throw "Tried to start from nonexisting state '" + startState + "'"; 
        }
        this.state = this.states[startState]; 
        var advance = function(m) {
            if (!m.state) return;
            var startTime = new Date().getTime();
            var result = m.state.advance(m);
            if (!result) {
                var name = m.state.name;
                m.state = null;
                throw "The advance of state '" + name + "' does not "
                      + "return correct state transition object";
            }
            if (m.debugMode) {
                console.log("HMOTINE: " + m.state.name + 
                        " " + JSON.stringify(m.state.getAllData()));
            }
            if (result.nextState === null) {
                m.state = null;
                return;
            }
            if (result.nextState) {
                m.state = m.states[result.nextState];
            }
            var elapsedTime = new Date().getTime() - startTime;
            var waitingTime = result.nextTime ? result.nextTime - elapsedTime : 0; 
            setTimeout(advance, waitingTime, m);                    
        };
        advance(this);
    }, 
            
    get: function(parameter) {
        return this.state.getS(parameter);
    },
    
    set: function(parameter, value) {
        this.state.setS(parameter, value);
    },
    
    clean: function() {
        this.states = {};
        this.state = null;
    },
    
    goto: function(nextState, nextTime) {
        if (nextState && !this.states[nextState]) {
            this.state = null;
            throw "Tried to switch to nonexisting '" + nextState + "'"; 
        }
        return { nextState: nextState, 
                 nextTime: nextTime };
    },
    
    keep: function(nextTime) {
        return { nextTime: nextTime };
    },
    
    end: function() {
        return { nextState: null };
    },
    
    running: function() {
        return !!this.state;
    },

    debugOn: function() {
        this.debugMode = true; 
    },
    
    debugOff: function() {
        this.debugMode = false; 
    },
    
    debugMode: false,
    
    states: {},
    
    state: null
};

module.exports = machine; 

